require 'rails_helper'


RSpec.describe CadastroBasicos::MunicipesController, :type => :controller do
  include Devise::Test::ControllerHelpers

  context "Get #index" do

    #https://stackoverflow.com/questions/27334887/how-to-write-rspec-testing-for-devise-authenticate-methods
    #let(:email) { "carloslima519@outlook.com" }
    #let(:warden_conditions) { { login: email } }
    login_admin

    it "should Sucess and render to index page" do

      admin = Admin.first
      PermissoesService.run({})

      sign_in admin

      get :index#, params: {id: 1}
      expect(response).to have_http_status(200)
      # expect(response).to render_template(:index)
    end
  end
end
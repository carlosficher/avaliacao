require "rails_helper"

describe "User signs in", type: :system do
  before do
    @user = create :admin
    visit new_admin_session_path
  end

  scenario "valid with correct credentials" do
    fill_in "admin_nmr_registro", with: @user.nmr_registro
    fill_in "admin_password", with: @user.password
    click_button "Entrar"

    expect(page).to have_text "Login efetuado com sucesso!"
  end


end
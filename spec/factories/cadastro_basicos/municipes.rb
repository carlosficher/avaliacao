FactoryBot.define do
  factory :cadastros_basicos_municipes do
    id {}
    nome_completo {"Amanda Dourado Lourenço"}
    cpf {"70328176109"}
    cns {"1234567"}
    dta_nascimento {"2000-01-24"}
    telefone {'62993437996'}
    status {1}
    status {true}
  end
end
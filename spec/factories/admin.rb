FactoryBot.define do
  factory(:admin) do
    id {1}
    email {"carloslima519@outlook.com"}
    created_at {"2022-04-01 18:53:33"}
    updated_at {"2022-05-10 02:34:32"}
    cpf {"70328176109"}
    name {"Carlo Eduardo de Lima"}
    nmr_registro {'carloslima519@outlook.com'}
    password {'70328176109'}
    password_confirmation {'70328176109'}
    date_birth {nil}
    date_register {nil}
    active {nil}
    kind {1}

  end
end

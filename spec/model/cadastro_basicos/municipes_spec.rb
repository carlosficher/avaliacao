require 'rails_helper'

RSpec.describe CadastroBasicos::Municipe, :type => :model do
  context "Validar Municipe" do
    it "que ele seja valido" do

      municipe = CadastroBasicos::Municipe.new(id: 1, nome_completo: "teste",  cpf: "70328176109",  cns: "222222222", email: "carloslima519@outlook.com", dta_nascimento: "2000-01-24", telefone: "62993437996", foto: nil, status: 1).tap(&:save)

      expect(municipe).to be_valid
    end

    it "que ele não seja valido" do

      municipe = CadastroBasicos::Municipe.new(id: 1, nome_completo: nil,  cpf: "70328176109",  cns: "222222222", email: "carloslima519@outlook.com", dta_nascimento: "2000-01-24", telefone: "62993437996", foto: nil, status: 1).tap(&:save)


      expect(municipe).to_not be_valid
    end
  end
end
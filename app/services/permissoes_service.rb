# frozen_string_literal: true
class PermissoesService < ApplicationService
  ACTION = :atualiza

  # Executa os métodos e cria um novo contracheque online
  def atualiza

    CadastroBasicos::TbperfilTbpermissao.destroy_all
    CadastroBasicos::Tbperfilusuario.destroy_all
    CadastroBasicos::Tbperfil.destroy_all
    CadastroBasicos::Tbpermissao.destroy_all

    CadastroBasicos::Tbperfil.create(id: 1, nome: 'Administrador', ativo: true, chave: 'administrador')
    CadastroBasicos::Tbperfil.create(id: 2, nome: 'Basica', ativo: true, chave: 'basica')

    CadastroBasicos::Tbpermissao.create(id: 1, classe: 'CadastroBasicos::Municipe', acao: 'manage',  ativo: true, chave: 'municipe', nome: 'municipe')
    CadastroBasicos::Tbpermissao.create(id: 2, classe: 'CadastroBasicos::Endereco', acao: 'manage',  ativo: true, chave: 'endereco', nome: 'endereco')
    CadastroBasicos::Tbpermissao.create(id: 14, classe: 'CadastroBasicos::TbperfilTbpermissao', acao: 'manage',  ativo: true, nome: 'Perfil Permissão')
    CadastroBasicos::Tbpermissao.create(id: 15, classe: 'CadastroBasicos::Tbperfilusuario', acao: 'manage',  ativo: true, nome: 'Perfil Usuário')
    CadastroBasicos::Tbpermissao.create(id: 16, classe: 'CadastroBasicos::Tbpermissao', acao: 'manage',  ativo: true, nome: 'Permissão')
    CadastroBasicos::Tbpermissao.create(id: 10, classe: 'CadastroBasicos::Tbperfil', acao: 'manage',  ativo: true, nome: 'perfil')
    CadastroBasicos::Tbpermissao.create(id: 17, classe: 'Admin', acao: 'manage',  ativo: true, nome: 'Admin')
    CadastroBasicos::Tbpermissao.create(id: 18, classe: 'AdminUsuario', acao: 'manage',  ativo: true, nome: 'Usuários')
    CadastroBasicos::Tbpermissao.create(id: 27, classe: 'Audit', acao: 'manage',  ativo: true, nome: 'Auditoria')

    administradores = Admin.where(email: ['carloslima519@outlook.com']).map(&:id)
    basicas = Admin.where(email: ['teste@gmail.com']).map(&:id)

    p '--------------------Criando permissoes--------------------'
    CadastroBasicos::Tbperfil.all.each do |perfil|
      case perfil.chave
      when 'administrador'
        p 'Criando permissoes Administrador'
        cria_perfil_usuario(administradores, perfil&.id)
        administrador.each do |permissao|
          CadastroBasicos::TbperfilTbpermissao.create(tbperfil_id: perfil.id, tbpermissao_id: permissao.id)
        end
      when 'basicas'
        p 'Criando permissoes basicas'
        cria_perfil_usuario(basicas, perfil&.id)
        basicas.each do |permissao|
          CadastroBasicos::TbperfilTbpermissao.create(tbperfil_id: perfil.id, tbpermissao_id: permissao.id)
        end
      end
    end

  end

  def administrador
    CadastroBasicos::Tbpermissao.all
  end

  def basicas
    classe = [
      'CadastroBasicos::Endereco'
    ]
    CadastroBasicos::Tbpermissao.where(classe: classe)
  end

  def cria_perfil_usuario(admins, perfil_id)
    admins.each do |admin|
      CadastroBasicos::Tbperfilusuario.create(tbperfil_id: perfil_id, admin_id: admin)
    end
  end

end

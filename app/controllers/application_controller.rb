module Current
  thread_mattr_accessor :admin
end

class ApplicationController < ActionController::Base
  include TranslateHelper
  include AlertsHelper
  layout "backoffice"

  before_action :set_parametros, except: [:create, :destroy, :update]
  # before_action :authenticate_admin! #do devise, para verificar se logou para acessar qualquer função

  skip_before_action :verify_authenticity_token

  around_action :set_current_user

  def set_current_user
    Current.admin = current_admin
    yield
  ensure
    # to address the thread variable leak issues in Puma/Thin webserver
    Current.admin = nil
  end


  def current_ability
    @current_ability ||= Ability.new(current_admin)
  end

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to backoffice_path, :notice => exception.message
  end

  def controller?(*controller)
    controller.include?(params[:controller])
  end

  # protect_from_forgery
  # rescue_from CanCan::AccessDenied do |exception|
  #   flash[:danger] = "Acesso negado, Você não esta autorizado a acessar esta pagina"
  #   redirect_to root_path
  # end

  # Adicionar item ao breadcrumb da pagina
  def add_breadcrumb(name, url = "#", icon = "", menu = name)
    @breadcrumbs ||= []
    url = eval(url) if url =~ /_path|_url|@/
    @breadcrumbs << [name, url, icon, menu]
  end

  # before_action do
  #   resource = controller_path.singularize.gsub("/", "_").to_sym
  #   method = "#{resource}_params"
  #   params[resource] &&= send(method) if respond_to?(method, true)
  # end

  # Remove Breadcrumb na tela
  def remove_breadcumb
    @breadcrumbs = []
  end

  # Redireciona para o início da aplicação quando não há permissão
  def redirect_to_permission(msg = 'Você não possui permissão!')
    redirect_to root_path, flash: { danger: msg }
  end




  def self.local_prefixes
    ['shared', controller_path]
  end




  private_class_method :local_prefixes

  private

  def set_parametros
    return if params[:format].to_s.eql?("json") || ["text/javascript", "application/json"].include?(request.format.to_s)
  end

  protected

  def authenticate_admin!(options = nil)
    return if [:sessions, :logins].include? controller_name.to_sym
    if admin_signed_in?
      super({ force: true })
    else
      redirect_to admin_session_path
    end
  end

end

# frozen_string_literal: true

class AuditsController < BackofficeController
  before_action :add_breadcrumb_modulo

  # GET /tbrankings
  # Visualizar Tbranking
  def index
    @auditable_types = Audit.distinct.pluck(:auditable_type)
    @q = Audit.where.not(user_id: nil).order(created_at: :desc).ransack(params[:q])
    @audits = @q.result.page(params[:page])
  end

  # GET /tbrankings/1
  # Visualizar Tbranking
  def show
    add_breadcrumb label_translate(:show)
  end

  # Retorna um resultado de consulta para o autocomplete


  private

  # Adiciona Breadcrumb geral para o controller
  def add_breadcrumb_modulo
    add_breadcrumb 'Auditoria', '#'
  end
end

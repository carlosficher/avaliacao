class AdminUsuariosController < BackofficeController
  before_action :autorizacao
  before_action :set_admin, only: [:show, :edit, :update, :destroy, :inativar_admin]
  # load_and_authorize_resource except: :create
  before_action :add_breadcrumb_modulo

  def index
    session[:cdg_estado] = params[:cdg_estado] || session[:cdg_estado]
    @q = AdminUsuario.all.ransack(params[:q])
    @admin = @q.result.page(params[:page])
  end

  def show
    add_breadcrumb label_translate(:show)
  end

  def new
    add_breadcrumb label_translate(:new)
    @admin = AdminUsuario.new
  end

  def edit
    add_breadcrumb label_translate(:edit)
  end

  def create
    @admin = AdminUsuario.new(admin_params)
    respond_to do |format|
      if @admin.save
        format.html { redirect_to @admin, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @admin }
        format.js {}
      else
        format.html { render :new }
        format.json { render json: @admin.errors, status: :unprocessable_entity }
        format.js { render 'shared/show_messages' }
      end
    end
  end

  def update
    if params[:admin_usuario][:password].blank? && params[:admin_usuario][:password_confirmation].blank?
      params[:admin_usuario].extract!(:password, :password_confirmation)
    end
    respond_to do |format|
      if @admin.update(admin_params)
        format.html { redirect_to @admin, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @admin }
        format.js {}
      else
        format.html { render :new }
        format.json { render json: @admin.errors, status: :unprocessable_entity }
        format.js { render 'shared/show_messages' }
      end
    end
  end

  def destroy
    @admin.destroy
  end

  def inativar_admin
    @admin.update!(active: false, email: "inativo" + @admin.email, password: 'inativo123')
  end



  private

  def set_admin
    @admin = AdminUsuario.find(params[:id])
  end

  def admin_params
    params.require(:admin_usuario).permit(:id, :email, :kind, :cpf, :name, :date_birth, :date_register, :active, :password, :reset_password_token,
                                          admin_cidades_attributes: [:id, :admin_id, :admin_usuario_id, :cidade_id, :_destroy],
                                          cadastro_basicos_tbperfilusuarios_attributes: [:id, :admin_id, :tbperfil_id, :_destroy],
                                          admin_cidade_attributes: [:id, :admin_id, :cidade_id, :_destroy],
                                          admins_attributes: [:id, :email, :_destroy])
  end

  def add_breadcrumb_modulo
    add_breadcrumb "Admin"
  end

  def autorizacao
    redirect_to_permission unless can? :manage, AdminUsuario
  end
end

# frozen_string_literal: true

class CadastroBasicos::TbperfisController < CadastroBasicos::ApplicationController
  before_action :autorizacao
  before_action :set_cadastro_basicos_tbperfil, only: %i(show edit update destroy)
  before_action :add_breadcrumb_modulo

  # GET /cadastro_basicos/tbperfis
  # Visualizar CadastroBasicos::Tbperfil
  def index

    @cadastro_basicos_tbperfis = CadastroBasicos::Tbperfil.all.page(params[:page])
  end

  # GET /cadastro_basicos/tbperfis/1
  # Visualizar CadastroBasicos::Tbperfil
  def show
    add_breadcrumb label_translate(:show)
  end

  # GET /cadastro_basicos/tbperfis/new
  # Novo CadastroBasicos::Tbperfil
  def new
    add_breadcrumb label_translate(:new)
    @cadastro_basicos_tbperfil = CadastroBasicos::Tbperfil.new
  end

  # GET /cadastro_basicos/tbperfis/1/edit
  # Editar CadastroBasicos::Tbperfil
  def edit
    add_breadcrumb label_translate(:edit)
  end

  # POST /cadastro_basicos/tbperfis
  # Criar CadastroBasicos::Tbperfil
  def create
    @cadastro_basicos_tbperfil = CadastroBasicos::Tbperfil.new(cadastro_basicos_tbperfil_params)
    @cadastro_basicos_tbperfil.save
  end

  # PATCH/PUT /cadastro_basicos/tbperfis/1
  # Atualizar CadastroBasicos::Tbperfil
  def update
    @cadastro_basicos_tbperfil.update(cadastro_basicos_tbperfil_params)
  end

  # DELETE /cadastro_basicos/tbperfis/1
  # Excluir CadastroBasicos::Tbperfil
  def destroy
    @cadastro_basicos_tbperfil.destroy
  end

  # Retorna um resultado de consulta para o autocomplete
  def autocomplete
    if params[:key].present?
      @lista_dados = CadastroBasicos::Tbperfil.find(params[:key])
    else
      @lista_dados = CadastroBasicos::Tbperfil.like(params[:q])
    end
    render json: @lista_dados.as_json
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cadastro_basicos_tbperfil
      @cadastro_basicos_tbperfil = CadastroBasicos::Tbperfil.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def cadastro_basicos_tbperfil_params
      params.require(:cadastro_basicos_tbperfil).permit(:nome, :ativo, :chave,
                                                        cadastro_basicos_tbperfil_tbpermissaos_attributes: [:id, :tbpermissao_id,
                                                                                                                :tbperfil_id,
                                                                                                                :_destroy],
                                                        cadastro_basicos_tbperfilusuarios_attributes: [:id, :admin_id,
                                                                                                            :tbperfil_id,
                                                                                                            :_destroy])
    end

    # Adiciona Breadcrumb geral para o controller
    def add_breadcrumb_modulo
      add_breadcrumb translate_model(CadastroBasicos::Tbperfil), cadastro_basicos_tbperfis_path
    end

 def autorizacao
   redirect_to_permission unless can? :manage, CadastroBasicos::Tbperfil
  end
end

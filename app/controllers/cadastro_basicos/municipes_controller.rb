# frozen_string_literal: true

class CadastroBasicos::MunicipesController < CadastroBasicos::ApplicationController
  before_action :set_cadastro_basicos_municipe, only: %i(show edit update destroy)
  before_action :add_breadcrumb_modulo

  # GET /cadastro_basicos/municipes
  # Visualizar CadastroBasicos::Municipe
  def index
    @q = CadastroBasicos::Municipe.eager_load(:cadastro_basicos_enderecos).ransack(params[:q])
    @cadastro_basicos_municipes = @q.result.page(params[:page])
  end

  # GET /cadastro_basicos/municipes/1
  # Visualizar CadastroBasicos::Municipe
  def show
    add_breadcrumb label_translate(:show)
  end

  # GET /cadastro_basicos/municipes/new
  # Novo CadastroBasicos::Municipe
  def new
    add_breadcrumb label_translate(:new)
    @cadastro_basicos_municipe = CadastroBasicos::Municipe.new
  end

  # GET /cadastro_basicos/municipes/1/edit
  # Editar CadastroBasicos::Municipe
  def edit
    add_breadcrumb label_translate(:edit)
  end

  # POST /cadastro_basicos/municipes
  # Criar CadastroBasicos::Municipe
  def create
    @cadastro_basicos_municipe = CadastroBasicos::Municipe.new(cadastro_basicos_municipe_params)
    if @cadastro_basicos_municipe.save
      redirect_to cadastro_basicos_municipes_path, success: translate_model(CadastroBasicos::Municipe) + flash_messages_translate(:successfully_created)
    else
      render action: "new"
    end

  end

  # PATCH/PUT /cadastro_basicos/municipes/1
  # Atualizar CadastroBasicos::Municipe
  def update

    if @cadastro_basicos_municipe.update(cadastro_basicos_municipe_params)
      redirect_to cadastro_basicos_municipes_path, success: translate_model(CadastroBasicos::Municipe) + flash_messages_translate(:successfully_created)
    else
      render action: "edit"
    end
  end

  # DELETE /cadastro_basicos/municipes/1
  # Excluir CadastroBasicos::Municipe
  def destroy
    @cadastro_basicos_municipe.destroy
  end

  # Retorna um resultado de consulta para o autocomplete
  def autocomplete
    if params[:key].present?
      @lista_dados = CadastroBasicos::Municipe.find(params[:key])
    else
      @lista_dados = CadastroBasicos::Municipe.like(params[:q])
    end
    render json: @lista_dados.as_json
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cadastro_basicos_municipe
      @cadastro_basicos_municipe = CadastroBasicos::Municipe.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def cadastro_basicos_municipe_params
      params.require(:cadastro_basicos_municipe).permit(:nome_completo, :cpf, :cns, :email, :dta_nascimento, :telefone, :foto, :status,
                                                        cadastro_basicos_enderecos_attributes: [:id, :cep, :logradouro, :bairro, :cidade, :uf, :_destroy])
    end

    # Adiciona Breadcrumb geral para o controller
    def add_breadcrumb_modulo
      add_breadcrumb translate_model(CadastroBasicos::Municipe), cadastro_basicos_municipes_path
    end
end

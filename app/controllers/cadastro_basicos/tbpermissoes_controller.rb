# frozen_string_literal: true

class CadastroBasicos::TbpermissoesController < CadastroBasicos::ApplicationController
  before_action :autorizacao
  before_action :set_cadastro_basicos_tbpermissao, only: %i(show edit update destroy)
  before_action :add_breadcrumb_modulo

  # GET /cadastro_basicos/tbpermissoes
  # Visualizar CadastroBasicos::Tbpermissao
  def index

    @cadastro_basicos_tbpermissoes = CadastroBasicos::Tbpermissao.all.page(params[:page])
  end

  # GET /cadastro_basicos/tbpermissoes/1
  # Visualizar CadastroBasicos::Tbpermissao
  def show
    add_breadcrumb label_translate(:show)
  end

  # GET /cadastro_basicos/tbpermissoes/new
  # Novo CadastroBasicos::Tbpermissao
  def new
    add_breadcrumb label_translate(:new)
    @cadastro_basicos_tbpermissao = CadastroBasicos::Tbpermissao.new
  end

  # GET /cadastro_basicos/tbpermissoes/1/edit
  # Editar CadastroBasicos::Tbpermissao
  def edit
    add_breadcrumb label_translate(:edit)
  end

  # POST /cadastro_basicos/tbpermissoes
  # Criar CadastroBasicos::Tbpermissao
  def create
    @cadastro_basicos_tbpermissao = CadastroBasicos::Tbpermissao.new(cadastro_basicos_tbpermissao_params)
    @cadastro_basicos_tbpermissao.save
  end

  # PATCH/PUT /cadastro_basicos/tbpermissoes/1
  # Atualizar CadastroBasicos::Tbpermissao
  def update
    @cadastro_basicos_tbpermissao.update(cadastro_basicos_tbpermissao_params)
  end

  # DELETE /cadastro_basicos/tbpermissoes/1
  # Excluir CadastroBasicos::Tbpermissao
  def destroy
    @cadastro_basicos_tbpermissao.destroy
  end

  # Retorna um resultado de consulta para o autocomplete
  def autocomplete
    if params[:key].present?
      @lista_dados = CadastroBasicos::Tbpermissao.find(params[:key])
    else
      @lista_dados = CadastroBasicos::Tbpermissao.like(params[:q])
    end
    render json: @lista_dados.as_json
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cadastro_basicos_tbpermissao
      @cadastro_basicos_tbpermissao = CadastroBasicos::Tbpermissao.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def cadastro_basicos_tbpermissao_params
      params.require(:cadastro_basicos_tbpermissao).permit(:classe, :acao, :nome, :permissao_pai, :ativo, :chave)
    end

    # Adiciona Breadcrumb geral para o controller
    def add_breadcrumb_modulo
      add_breadcrumb translate_model(CadastroBasicos::Tbpermissao), cadastro_basicos_tbpermissoes_path
    end

  def autorizacao
    redirect_to_permission unless can? :manage, CadastroBasicos::Tbpermissao
  end
end

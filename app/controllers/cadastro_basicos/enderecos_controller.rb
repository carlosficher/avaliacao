# frozen_string_literal: true

class CadastroBasicos::EnderecosController < CadastroBasicos::ApplicationController
  before_action :set_cadastro_basicos_endereco, only: %i(show edit update destroy)
  before_action :add_breadcrumb_modulo

  # GET /cadastro_basicos/enderecos
  # Visualizar CadastroBasicos::Endereco
  def index

    @cadastro_basicos_enderecos = CadastroBasicos::Endereco.all.page(params[:page])
  end

  # GET /cadastro_basicos/enderecos/1
  # Visualizar CadastroBasicos::Endereco
  def show
    add_breadcrumb label_translate(:show)
  end

  # GET /cadastro_basicos/enderecos/new
  # Novo CadastroBasicos::Endereco
  def new
    add_breadcrumb label_translate(:new)
    @cadastro_basicos_endereco = CadastroBasicos::Endereco.new
  end

  # GET /cadastro_basicos/enderecos/1/edit
  # Editar CadastroBasicos::Endereco
  def edit
    add_breadcrumb label_translate(:edit)
  end

  # POST /cadastro_basicos/enderecos
  # Criar CadastroBasicos::Endereco
  def create
    @cadastro_basicos_endereco = CadastroBasicos::Endereco.new(cadastro_basicos_endereco_params)
    @cadastro_basicos_endereco.save
  end

  # PATCH/PUT /cadastro_basicos/enderecos/1
  # Atualizar CadastroBasicos::Endereco
  def update
    @cadastro_basicos_endereco.update(cadastro_basicos_endereco_params)
  end

  # DELETE /cadastro_basicos/enderecos/1
  # Excluir CadastroBasicos::Endereco
  def destroy
    @cadastro_basicos_endereco.destroy
  end

  # Retorna um resultado de consulta para o autocomplete
  def autocomplete
    if params[:key].present?
      @lista_dados = CadastroBasicos::Endereco.find(params[:key])
    else
      @lista_dados = CadastroBasicos::Endereco.like(params[:q])
    end
    render json: @lista_dados.as_json
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cadastro_basicos_endereco
      @cadastro_basicos_endereco = CadastroBasicos::Endereco.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def cadastro_basicos_endereco_params
      params.require(:cadastro_basicos_endereco).permit(:cep, :logradouro, :complemento, :bairro, :cidade, :uf, :cod_ibge)
    end

    # Adiciona Breadcrumb geral para o controller
    def add_breadcrumb_modulo
      add_breadcrumb translate_model(CadastroBasicos::Endereco), cadastro_basicos_enderecos_path
    end
end

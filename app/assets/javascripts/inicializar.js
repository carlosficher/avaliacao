// $(document).on('nested:fieldAdded', function (el) {
//     resetarComponents()
// });
// $(document).on('cocoon:after-insert', function (el) {
//     resetarComponents()
// });


function initialize() {
    const App = window.App || (window.App = {});
    try {
        App.configDatatable();
    } catch (e) {
        console.warn("Erro inicializar o datatable");
        console.log(e);
    }
    try {
        App.dateRangePicker();
    } catch (e) {
        console.warn("Erro inicializar as date range");
        console.log(e);
    }
    try {
        App.setMasks();
    } catch (e) {
        console.warn("Erro inicializar as máscaras");
        console.log(e);
    }

    $('.chosen-select').chosen({width: "100%"});

    try {
        App.configSelect2();
    } catch (e) {
        console.warn("Erro inicializar o select2 ajax e simples");
        console.log(e);
    }


    try {
        $('.select2').select2();
    } catch (e) {
        console.warn("Erro inicializar o select2");
        console.log(e);
    }

    try {
        $('.tools').remove();
    } catch (e) {
        console.warn("Erro ao remover tools jquery brackts");
        console.log(e);
    }
}

function resetarComponents() {
    const App = window.App || (window.App = {});
    try {
        App.dateRangePicker();
    } catch (e) {
        console.warn("Erro inicializar as date range");
        console.log(e);
    }
    try {
        App.setMasks();
    } catch (e) {
        console.warn("Erro inicializar as máscaras");
        console.log(e);
    }
    try {
        $('select.select2').select2();
    } catch (e) {
        console.warn("Erro inicializar o select2");
        console.log(e);
    }
}

$(document).on('turbolinks:load', function () {
    try {
        if ($('.breadcrumb-item.active').data() != undefined) {
            var breadcrumb_id = $('.breadcrumb-item.active').data().breadcrumb
            $('.nav-item').removeClass('menu-open');
            $('.nav-link').removeClass('active');
            $('.nav.nav-treeview').attr('style', 'display: none');
            var tag_a = $('.breadcrumb-' + breadcrumb_id)
            tag_a.addClass('active')
            tag_a.closest('.nav.nav-treeview').attr('style', 'display: block');
            tag_a.closest('.nav-item').parent().closest('.nav-item').addClass('menu-open');
            tag_a.closest('.nav-link').parent().closest('.nav-link').addClass('active');
        }
    } catch (e) {
    }

    try {
        $(".select-estado").on("change.select2", function (e) {
            $(e.target).closest("form").submit();
        });
    } catch (e) {
        console.warn("Erro ao selecionar estado");
        console.log(e);
    }

    const App = window.App || (window.App = {});


    App.select2Params = (form_id) => {
        $form = $('#' + form_id);
        // var formdata = JSON.stringify(form_array_serialize);
        var form_params = $form.find('.parametro_select2');

        var result = {};
        $.map(form_params, function (n, i) {
            var selectName = document.getElementsByName(n.name);
            if (selectName.length > 0) {
                var name = n['name'].split('[').slice(-1).join(' ').replace(']', '');
                result[name] = n['value'];
                // result[name] = selectName[0].value;
            }
        });
        return result;
    };
    App.configSelect2 = () => {
        // Select2 Ajax
        $('.select2ajax').each(function () {
            if ($(this).data("select2") === undefined) {
                App.applySelect2($(this));
            }
        });

        // Select2 initialization
        $('.select2').each(function () {
            if ($(this).data("select2") === undefined) {
                /* Ação para seleção do órgão */
                try {
                    $(this).select2({
                        placeholder: 'Digite para Pesquisar',
                        language: "pt-BR"
                    });
                } catch (e) {
                    console.warn("Erro ao carregar select2");
                    console.log(e);
                }
            }
        });
    }
    App.select2ajaxFull($('.select2ajaxFull'));
    App.applySelect2 = (el) => {
        const url = el.data('url');
        const form_id = el.data('formId');
        var multiple = el.attr('multiple');
        var minimumInput = el.data('param-minimumInput');
        if (minimumInput == null) {
            minimumInput = 0;
        }
        var _select2 = el.select2({
            placeholder: el.attr('placeholder'),
            allowClear: true,
            minimumInputLength: minimumInput,
            quietMillis: 500,
            deplay: 500,
            multiple: multiple,
            ajax: {
                url: url,
                dataType: 'json',
                quietMillis: 500,
                deplay: 500,
                data(term) {
                    var jsonParams = App.select2Params(form_id) || {};
                    jsonParams.q = term;
                    const paramName = el.data('param-name');
                    const paramValue = el.data('param-value');

                    if (paramName != null && paramValue != null) jsonParams[paramName] = paramValue;

                    return jsonParams;
                },
                results(data) {

                    const _results = [];
                    const _id_name = el.attr('id_name') || 'id';
                    const _text_name = el.attr('text_name') || 'text';

                    $(data).each(function () {
                        _results.push({id: this[_id_name], text: this[_text_name]})
                    });

                    return {results: _results};
                },
            },
            initSelection(element, callback) {
                const id = $(element).val();
                if (id === '') return;

                $.ajax(`${url}?key=${id}`, {
                    dataType: 'json',
                    data: App.select2Params(form_id)
                })
                    .done((data) => {

                        const _id_name = el.attr('id_name') || 'id';
                        const _text_name = el.attr('text_name') || 'text';

                        if (data == null) {
                            callback({id: '', text: ''});
                        } else if (data.constructor === Array) {
                            if (data.length > 0) {
                                callback({id: data[0][_id_name], text: data[0][_text_name]});
                            } else {
                                callback({id: '', text: ''});
                            }
                        } else {
                            callback({id: data[_id_name], text: data[_text_name]});
                        }

                    });
            },
        });

        return _select2;
    }
    App.configBootstrapDualListbox = () => {
        // $(".dual_select_br").bootstrapDualListbox({
        //     selectorMinimalHeight: 320,
        //     filterTextClear: 'Mostrar todos',
        //     filterPlaceHolder: 'Filtro',
        //     moveSelectedLabel: 'Mover selecionado',
        //     moveAllLabel: 'Mover todos',
        //     removeSelectedLabel: 'Remover selecionado',
        //     removeAllLabel: 'Remover todos',
        //     infoText: 'Mostrando todos {0}',                                                     // text when all options are visible / false for no info text
        //     infoTextFiltered: '<span class="label label-warning">Filtrado</span> {0} de {1}',   // when not all of the options are visible due to the filter
        //     infoTextEmpty: 'Lista vazia'                                                        // when there are no options present in the list
        // });
        //
        // $(".predio_uni_br").bootstrapDualListbox({
        //     selectorMinimalHeight: 320,
        //     filterPlaceHolder: 'Filtrar unidades'
        // });
    }


    initialize();
});
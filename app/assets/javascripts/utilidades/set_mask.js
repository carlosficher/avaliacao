(function () {
    const App = window.App || (window.App = {});

    App.setMasks = () => {
        // Masks
        $('.cpf').mask('000.000.000-00');
        $(".cnpj").mask("99.999.999/9999-99");
        $('.money').mask('000.000.000.000.000,00', {reverse: true});
        $('.hour').mask('00:00');
        $('.cep').mask('00000-000');
        $('.telefone').mask('(00) 0000-0009');
        $('.celular').mask('(00) 00000-0009');
        $('.data-format').mask('00/00/0000', {reverse: false});
        $('.peso').mask("000.00", {reverse: true});
        $('.altura').mask("0.00", {reverse: true});
        $('.input-decimal').mask("###0.00", {reverse: true});
    };
}).call(this);

(function () {
    const App = window.App || (window.App = {});

    App.dateRangePicker = () => {
        moment.locale('pt-br');
        $('.date-picker-single').each(function () {
            var value = $(this).val();
            if (value !== '') {
                $(this).daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    autoUpdateInput: false,
                    startDate: $(this).val(),
                    opens: 'center',
                    locale: {
                        cancelLabel: 'Fechar'
                    }
                });
            } else {
                $(this).daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    autoUpdateInput: false,
                    opens: 'center',
                    locale: {
                        cancelLabel: 'Fechar'
                    }
                });
            }
        });

        $('.date-picker-single').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY'));
        });

        $('.date-picker-year').each(function () {
            var value = $(this).val();
            if (value !== '') {
                $(this).daterangepicker({
                    singleDatePicker: true,
                    locale: {
                        format: 'yy'
                    },
                    startDate: $(this).val().format('YYYY'),
                    opens: 'center'
                });
            } else {
                $(this).daterangepicker({
                    singleDatePicker: true,
                    locale: {
                        format: 'yy'
                    },
                    startDate: moment().format('YYYY'),
                    opens: 'center'
                });
            }

        });

    }
}).call(this);

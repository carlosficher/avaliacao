// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.


//= require jquery
//= require jquery-ui
//= require jquery_ujs
//= require activestorage
//= require turbolinks
//= require adminlte/plugins/bootstrap/js/bootstrap.bundle.min
//= require adminlte/plugins/jquery-mousewheel/jquery.mousewheel
//= require adminlte/plugins/raphael/raphael.min
//= require adminlte/plugins/jquery-mapael/jquery.mapael.min
//= require adminlte/plugins/jquery-mapael/maps/usa_states.min
//= require adminlte/plugins/datatables/jquery.dataTables.min
//= require adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min
//= require adminlte/plugins/datatables-responsive/js/dataTables.responsive.min
//= require adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min
//= require adminlte/plugins/datatables-buttons/js/dataTables.buttons.min
//= require adminlte/plugins/datatables-buttons/js/buttons.bootstrap4.min
//= require adminlte/plugins/jszip/jszip.min
//= require adminlte/plugins/pdfmake/pdfmake.min
//= require adminlte/plugins/pdfmake/vfs_fonts
//= require adminlte/plugins/datatables-buttons/js/buttons.html5.min
//= require adminlte/plugins/datatables-buttons/js/buttons.print.min
//= require adminlte/plugins/datatables-buttons/js/buttons.colVis.min
//= require adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min
//= require adminlte/plugins/sweetalert2/sweetalert2
//= require adminlte/plugins/toastr/toastr.min
//= require adminlte/plugins/bs-stepper/js/bs-stepper.min
//= require select2/js/select2.full
//= require select2/select2.min.js
//= require_tree ./component
//= require chosen/chosen.jquery.js
//= require bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min
//= require jquery.mask
//= require moment/moment.min
//= require moment/moment-with-locales.min
//= require daterangepicker/daterangepicker
//= require adminlte/dist/js/adminlte
//= require adminlte/js/demo
//= require cocoon
//= require jquery-bracket
//= require_tree ./utilidades
//= require inicializar.js


# frozen_string_literal: true

class Audit < ApplicationRecord
  audited
  # main config ...............................................................
  self.table_name = "audits"
  # extends ...................................................................
  # includes ..................................................................
  # security (i.e. attr_accessible) ...........................................
  # relationships .............................................................
  belongs_to :admin_usuario, class_name: 'AdminUsuario', foreign_key: :user_id
  # validations ...............................................................
  #validates_presence_of  :classificacao_id, :ponto
  # callbacks .................................................................
  # scopes ....................................................................
  # Scope para consulta
  # additional config .........................................................
  # class methods .............................................................
  # public instance methods ...................................................
  # protected instance methods ................................................
  # private instance methods ..................................................
  # codigo e nome

end

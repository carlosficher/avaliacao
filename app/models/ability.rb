# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(admin)
    admin ||= Admin.new
    user = AdminUsuario.user_atual
    can :manage, Backoffice::DashboardController
    # # if user == "gestor"
    #   can :manage,:all
    # # end

    unless user.nil?
      CadastroBasicos::Tbpermissao.chaves_permissoes_pessoa(user.id).each do |permissao|
        permission_c = permissao.classe.constantize rescue nil
        if permission_c
          can permissao.acao.to_sym, permissao.classe.constantize
          if [:new].include?(permissao.acao.to_sym)
            can :create, permissao.classe.constantize
          elsif [:edit].include?(permissao.acao.to_sym)
            can :update, permissao.classe.constantize
          elsif [:index].include?(permissao.acao.to_sym)
            can :show, permissao.classe.constantize
          end
        elsif permissao.classe && permissao.acao.to_s == 'manage'
          can :manage, permissao.classe.to_sym
        end
      end
    end





    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not loggedListagem de beneficiários > Gerar (Somente para processos com escritura gerada) in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end

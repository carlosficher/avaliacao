# frozen_string_literal: true

class CadastroBasicos::Tbperfil < ApplicationRecord
  audited
  # main config ...............................................................
  self.table_name = "tbperfis"
  # extends ...................................................................
  # includes ..................................................................
  # security (i.e. attr_accessible) ...........................................
  # relationships .............................................................
  
    # validations ...............................................................
    #validates_presence_of  :nome, :ativo, :chave
    # callbacks .................................................................
    # scopes ....................................................................

  has_many :cadastro_basicos_tbperfil_tbpermissaos, :class_name => 'CadastroBasicos::TbperfilTbpermissao',
           foreign_key: :tbperfil_id
  has_many :cadastro_basicos_tbpermissaos, :class_name => 'CadastroBasicos::Tbpermissao', through: :cadastro_basicos_tbperfil_tbpermissaos
  accepts_nested_attributes_for :cadastro_basicos_tbperfil_tbpermissaos, allow_destroy: true


  has_many :cadastro_basicos_tbperfilusuarios, :class_name => 'CadastroBasicos::Tbperfilusuario',
           foreign_key: :tbperfil_id
  has_many :admins, :class_name => 'Admin', through: :cadastro_basicos_tbperfilusuarios
  accepts_nested_attributes_for :cadastro_basicos_tbperfilusuarios, allow_destroy: true



  # Scope para consulta
    scope :like, -> (value) do
      where(<<-SQL.squish, q: "%#{value.upcase}%").order(:descricao).limit(TOTAL_REGISTROS)
       upper(#{SCHEMA_NAME}.fu_sem_acentos(descricao)) like upper(#{SCHEMA_NAME}.fu_sem_acentos(:q))
          or to_char(id) like (:q)
      SQL
    end

    # additional config .........................................................
        # class methods .............................................................
    # public instance methods ...................................................
    # protected instance methods ................................................
    # private instance methods ..................................................
    # codigo e nome
    def codigo_descricao
      "#{self.descricao&.upcase} (#{self.id})"
    end

  end

# frozen_string_literal: true

class CadastroBasicos::Municipe < ApplicationRecord
  audited
  # main config ...............................................................
  self.table_name = "tbmunicipes"
  # extends ...................................................................
  # includes ..................................................................
  include RemoveFormatoConcern
  include ValidaCpfCnpjConcern
  # security (i.e. attr_accessible) ...........................................
  # relationships .............................................................
  has_many :cadastro_basicos_enderecos, :class_name => 'CadastroBasicos::Endereco',
           foreign_key: :municipes_id, dependent: :destroy
  accepts_nested_attributes_for :cadastro_basicos_enderecos, allow_destroy: true



  attr_accessor :foto
  has_one_attached :foto, :dependent => :destroy

  # validations ...............................................................
  validates_presence_of :nome_completo, :cpf, :cns, :email, :dta_nascimento, :telefone, :foto, :status
  validates_length_of :cns, :minimum => 5
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
  validates :dta_nascimento,
            inclusion: { in: 65.years.ago..18.years.ago }
  # callbacks .................................................................
  # scopes ....................................................................

  # Scope para consulta
  scope :like, -> (value) do
    where(<<-SQL.squish, q: "%#{value.upcase}%").order(:descricao).limit(TOTAL_REGISTROS)
       upper(#{SCHEMA_NAME}.fu_sem_acentos(descricao)) like upper(#{SCHEMA_NAME}.fu_sem_acentos(:q))
          or to_char(id) like (:q)
    SQL
  end



  # additional config .........................................................
  # class methods .............................................................
  # public instance methods ...................................................
  # protected instance methods ................................................
  # private instance methods ..................................................
  # codigo e nome
  def codigo_descricao
    "#{self.descricao&.upcase} (#{self.id})"
  end

end

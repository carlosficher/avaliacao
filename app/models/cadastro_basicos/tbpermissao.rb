# frozen_string_literal: true

class CadastroBasicos::Tbpermissao < ApplicationRecord
  audited
  # main config ...............................................................
  self.table_name = "tbpermissoes"
  # extends ...................................................................
  # includes ..................................................................
  # security (i.e. attr_accessible) ...........................................
  # relationships .............................................................

  has_many :cadastro_basicos_tbperfil_tbpermissaos, :class_name => 'CadastroBasicos::TbperfilTbpermissao',
           foreign_key: :tbpermissao_id
  has_many :cadastro_basicos_tbperfil, :class_name => 'CadastroBasicos::Tbperfil', through: :cadastro_basicos_tbperfil_tbpermissaos
  accepts_nested_attributes_for :cadastro_basicos_tbperfil_tbpermissaos, allow_destroy: true



  # validations ...............................................................
    #validates_presence_of  :classe, :acao, :nome, :permissao_pai, :ativo, :chave
    # callbacks .................................................................
    # scopes ....................................................................

    # Scope para consulta
    scope :like, -> (value) do
      where(<<-SQL.squish, q: "%#{value.upcase}%").order(:descricao).limit(TOTAL_REGISTROS)
       upper(#{SCHEMA_NAME}.fu_sem_acentos(descricao)) like upper(#{SCHEMA_NAME}.fu_sem_acentos(:q))
          or to_char(id) like (:q)
      SQL
    end

  def self.chaves_permissoes_pessoa(user_id, cdg_campeonato = 1)
    self.permissoes_usuario(user_id, cdg_campeonato)
  end

  def self.permissoes_usuario(user_id, cdg_campeonato = 1)
    self.joins(cadastro_basicos_tbperfil: [:cadastro_basicos_tbperfil_tbpermissaos])
        .joins(cadastro_basicos_tbperfil: [:cadastro_basicos_tbperfilusuarios])
        .joins("inner join admins a on a.id = #{user_id.to_i}")
        .where("tbperfilusuarios.admin_id is not null and tbperfilusuarios.admin_id = #{user_id.to_i}")
        .distinct
  end





    # additional config .........................................................
        # class methods .............................................................
    # public instance methods ...................................................
    # protected instance methods ................................................
    # private instance methods ..................................................
    # codigo e nome
    def codigo_descricao
      "#{self.nome&.upcase} (#{self.id})"
    end

  end

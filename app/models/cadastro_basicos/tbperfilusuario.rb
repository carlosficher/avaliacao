# frozen_string_literal: true

class CadastroBasicos::Tbperfilusuario < ApplicationRecord
  audited
  # main config ...............................................................
  self.table_name = "tbperfilusuarios"
  # extends ...................................................................
  # includes ..................................................................
  # security (i.e. attr_accessible) ...........................................
  # relationships .............................................................
      belongs_to :cadastro_basicos_tbperfil, :class_name => 'CadastroBasicos::Tbperfil',
            foreign_key: :tbperfil_id, optional: true
      belongs_to :admin,  class_name: 'admin',
            foreign_key: :admin, optional: true
  
    # validations ...............................................................
    #validates_presence_of  :tbperfil, :admin, :cdg_campeonato
    # callbacks .................................................................
    # scopes ....................................................................

    # Scope para consulta
    scope :like, -> (value) do
      where(<<-SQL.squish, q: "%#{value.upcase}%").order(:descricao).limit(TOTAL_REGISTROS)
       upper(#{SCHEMA_NAME}.fu_sem_acentos(descricao)) like upper(#{SCHEMA_NAME}.fu_sem_acentos(:q))
          or to_char(id) like (:q)
      SQL
    end

    # additional config .........................................................
        # class methods .............................................................
    # public instance methods ...................................................
    # protected instance methods ................................................
    # private instance methods ..................................................
    # codigo e nome
    def codigo_descricao
      "#{self.descricao&.upcase} (#{self.id})"
    end


  def self.permite_faixa_preta?(user_id)
    CadastroBasicos::Tbperfilusuario.joins(:cadastro_basicos_tbperfil)
                                    .where("tbperfis.chave in ('administrador', 'nacional')")
                                    .where(admin_id: user_id).present?

  end


end

module AdminInfo
  def current_admin
    Thread.current[:adnin]
  end

  def self.current_admin=(admin)
    Thread.current[:adnin] = admin
  end
end
class Admin < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :authentication_keys => {email: false, nmr_registro: false}

  has_one :admin_usuario, foreign_key: :id



  def login
    @login || self.nmr_registro || self.email
  end


  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if (login = conditions.delete(:login))
      where(conditions.to_h).where(["lower(nmr_registro) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    elsif conditions.has_key?(:nmr_registro) || conditions.has_key?(:email)
      where(["lower(nmr_registro) = :value OR lower(email) = :value", { :value => conditions.first[1] }]).first
    end
  end

end

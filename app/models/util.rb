class Util
  # :need-doc:

  # :need-doc:


  def self.translate_attr(class_name, attr_name)
    I18n.t("activerecord.attributes.#{class_name.to_s.underscore}.#{attr_name}")
  end




  def self.translate_enum_name(class_name, enum_name, enum_value)
    return nil unless enum_value
    I18n.t("activerecord.attributes.#{class_name.to_s.underscore}.#{enum_name}.#{enum_value}")
  end

  # Retorna o nome do model traduzido
  #
  # @param class_name [String|Symbol] o nome do model
  #
  # @return [String] tradução do model
  def self.translate_model(class_name)
    I18n.t("activerecord.models.#{class_name.to_s.underscore.downcase}")
  end



end
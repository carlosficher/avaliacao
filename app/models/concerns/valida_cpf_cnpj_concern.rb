# frozen_string_literal: true

require 'active_support/concern'

module ValidaCpfCnpjConcern
  extend ActiveSupport::Concern

  included do
    before_validation :cpf_cnpj
  end

  private

  def cpf_cnpj
    valida_cpf_cnpj(self.cnpj, 'cnpj') if self.attributes['cnpj']
    valida_cpf_cnpj(self.cpf, 'cpf') if self.attributes['cpf']
  end

  def valida_cpf_cnpj(value, column)
    if value.present?
      size = value.to_s.size
      if size == 11
        cpf = CPF.new(value)
        self.errors.add(column, 'não é válido') unless cpf.valid?
      elsif size == 14
        cnpj = CNPJ.new(value)
        self.errors.add(column, 'não é válido') unless cnpj.valid?
      else
        self.errors.add(column, 'não é válido')
      end
    end
  end

end

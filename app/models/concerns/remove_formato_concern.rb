# frozen_string_literal: true

require 'active_support/concern'

module RemoveFormatoConcern
  extend ActiveSupport::Concern

  included do
    before_validation :remove_formato
  end

  private

  def remove_formato
    self.cnpj = self.cnpj.to_s.gsub(".", "").gsub("/", "").gsub("-", "") if self.attributes['cnpj']
    self.cpf = self.cpf.to_s.gsub(".", "").gsub("-", "") if self.attributes['cpf']
    self.nmr_cpf = self.nmr_cpf.to_s.gsub(".", "").gsub("-", "") if self.attributes['nmr_cpf']
    self.cep = self.cep.to_s.gsub(".", "").gsub("-", "") if self.attributes['cep']
    self.nmr_contato = self.nmr_contato.to_s.gsub("(", "").gsub(")", "").gsub("-", "").gsub(" ", "") if self.attributes['nmr_contato']
    self.telefone = self.telefone.to_s.gsub("(", "").gsub(")", "").gsub("-", "").gsub(" ", "") if self.attributes['telefone']
  end

end

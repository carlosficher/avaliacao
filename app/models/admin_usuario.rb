class AdminUsuario < ApplicationRecord
  audited
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable

  self.table_name = 'admins'

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validates_presence_of :password, length: { in: 6..20 }, on: :create
  validates_presence_of :name, length: { minimum: 2 }


  has_many :cadastro_basicos_tbperfilusuarios, :class_name => 'CadastroBasicos::Tbperfilusuario',
           foreign_key: :admin_id, dependent: :destroy
  has_many :admins, :class_name => 'Admin', through: :cadastro_basicos_tbperfilusuarios
  accepts_nested_attributes_for :cadastro_basicos_tbperfilusuarios, allow_destroy: true

  def self.set_current_user
    Current.admin.id
  end

  def self.user_atual
    AdminUsuario.find(Current.admin.id)
  end

  enum kind: { comum: 0, gestor: 1 }



  private




end

json.extract! cadastro_basicos_municipe, :id, :nome_completo, :cpf, :cns, :email, :dta_nascimento, :telefone, :foto, :status, :created_at, :updated_at
json.url cadastro_basicos_municipe_url(cadastro_basicos_municipe, format: :json)

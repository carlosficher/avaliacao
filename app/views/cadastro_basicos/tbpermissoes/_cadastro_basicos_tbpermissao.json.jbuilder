json.extract! cadastro_basicos_tbpermissao, :id, :classe, :acao, :nome, :entidade, :permissao_pai, :ativo, :chave, :created_at, :updated_at
json.url cadastro_basicos_tbpermissao_url(cadastro_basicos_tbpermissao, format: :json)

module ApplicationHelper

  def remover_acentos
    return self if self.blank?
    texto = self
    texto = texto.gsub(/(á|à|ã|â|ä)/, 'a').gsub(/(é|è|ê|ë)/, 'e').gsub(/(í|ì|î|ï)/, 'i').gsub(/(ó|ò|õ|ô|ö)/, 'o').gsub(/(ú|ù|û|ü)/, 'u')
    texto = texto.gsub(/(Á|À|Ã|Â|Ä)/, 'A').gsub(/(É|È|Ê|Ë)/, 'E').gsub(/(Í|Ì|Î|Ï)/, 'I').gsub(/(Ó|Ò|Õ|Ô|Ö)/, 'O').gsub(/(Ú|Ù|Û|Ü)/, 'U')
    texto = texto.gsub(/ñ/, 'n').gsub(/Ñ/, 'N')
    texto = texto.gsub(/ç/, 'c').gsub(/Ç/, 'C')
    texto
  end

  def retirar_acento
    return self if self.blank?
    texto = self
    texto.tr(
      "ÀÁÂÃÄÅàáâãäåĀāĂăĄąÇçĆćĈĉĊċČčÐðĎďĐđÈÉÊËèéêëĒēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħÌÍÎÏìíîïĨĩĪīĬĭĮįİıĴĵĶķĸĹĺĻļĽľĿŀŁłÑñŃńŅņŇňŉŊŋÒÓÔÕÖØòóôõöøŌōŎŏŐőŔŕŖŗŘřŚśŜŝŞşŠšſŢţŤťŦŧÙÚÛÜùúûüŨũŪūŬŭŮůŰűŲųŴŵÝýÿŶŷŸŹźŻżŽž",
      "AAAAAAaaaaaaAaAaAaCcCcCcCcCcDdDdDdEEEEeeeeEeEeEeEeEeGgGgGgGgHhHhIIIIiiiiIiIiIiIiIiJjKkkLlLlLlLlLlNnNnNnNnnNnOOOOOOooooooOoOoOoRrRrRrSsSsSsSssTtTtTtUUUUuuuuUuUuUuUuUuUuWwYyyYyYZzZzZz"
    )
    texto
  end

  def pdf_image_tag(image, options = {})
    options[:src] = Rails.root.join('public', 'images', image)
    tag(:img, options)
  end

  # Formata CPF e CNPJ
  def mascara_cpf_cnpj(numero_documento)
    if numero_documento.to_s.length == 11
      CPF.new(numero_documento).formatted
    elsif numero_documento.to_s.length == 14
      CNPJ.new(numero_documento).formatted
    elsif numero_documento.to_s.length > 11
      CNPJ.new(numero_documento.to_s.rjust(14, '0')).formatted
    else
      numero_documento
    end
  end

  # Cria um link para visualizar
  # @param path [String] path de visualização
  def link_to_click(path, options = {})
    link_to path,
            class: "#{options[:class] || 'btn btn-info btn-xs'}",
            title: options[:title],
            remote: options[:remote] || false,
            style: "#{options[:class] || 'color: #FFF; float: none;'}" do
      content_tag :span, "#{options[:label] || ''}",
                  class: "#{options[:class_label] || 'color: #FFF; float: none;'}" do
        label_tag :span, options[:label], class: "small_label" if options[:label]
      end
    end
  end

  #
  # <button type="button" class="btn btn-default btn-sm" >
  #     Launch demo modal
  # </button>

  def link_to_modal_full(path, options = {})
    link_to path,
            class: "btn btn-info  #{options[:class] || 'btn-xs'}",
            title: options[:title] || "Visualizar",
            data: { toggle: 'modal', target: '#modal-fill' },
            remote: options[:remote] || false,
            target: options[:target] || '_self',
            style: "color: #FFF; float: none;" do
      "#{ content_tag :span, class: options[:icone] || 'fa fa-search' do
        ''
      end} #{options[:label] if options.present? && options[:label]}".html_safe
    end
  end

  # Cria um link para visualizar
  # @param path [String] path de visualização
  def link_to_visualizar(path, options = {})
    link_to path,
            class: "btn btn-#{options[:color_class] || 'info' }  #{options[:class] || 'btn-sm'}",
            title: options[:title] || "Visualizar",
            remote: options[:remote] || false,
            target: options[:target] || '_self',
            style: "color: ##{options[:color] || 'FFF' }; float: none;" do
      "#{ content_tag :span, class: options[:icone] || 'fa fa-search' do
        ''
      end} #{options[:label] if options.present? && options[:label]}".html_safe
    end
  end

  def link_to_money(path, options = {})
    link_to path,
            class: "btn btn-success  #{options[:class] || 'btn-xs'}",
            title: options[:title] || "Visualizar",
            remote: options[:remote] || false,
            target: options[:target] || '_self',
            style: "color: #FFF; float: none;" do
      "#{ content_tag :span, class: 'fa fa-usd' do
        ''
      end} #{options[:label] if options.present? && options[:label]}".html_safe
    end
  end

  def link_to_imprimir(path, options = {})
    link_to path,
            class: "btn btn-success  #{options[:class] || 'btn-xs'}",
            title: options[:title] || "Visualizar",
            remote: options[:remote] || false,
            target: options[:target] || '_self',
            style: "color: #FFF; float: none;" do
      "#{ content_tag :span, class: 'fa fa-print' do
        ''
      end} #{options[:label] if options.present? && options[:label]}".html_safe
    end
  end

  def link_to_imprimir_onchange(path, options = {})
    link_to path,
            class: "btn btn-success  #{options[:class] || 'btn-xs'}",
            title: options[:title] || "Visualizar",
            remote: options[:remote] || false,
            target: options[:target] || '_self',
            style: "color: #FFF; float: none;" do
      "#{ content_tag :span, class: 'fa fa-print' do
        ''
      end} #{options[:label] if options.present? && options[:label]}".html_safe
    end
  end

  # onclick: "modal_espera()",
  def link_to_onclick(path, options = {})
    link_to path,
            class: "btn btn-#{options[:class_color] || 'success'}  #{options[:class] || 'btn-xs'}",
            title: options[:title] || "Cobrado?",
            remote: options[:remote] || false,
            onclick: options[:name_function] || "",
            target: options[:target] || '_self',
            style: "color: #FFF; float: none;" do
      "#{ content_tag :span, options[:icone] || 'btn-xs' do
        "#{" "}#{options[:title] || "Cobrado?"}"
      end} #{options[:label] if options.present? && options[:label]}".html_safe
    end
  end

  # <%= link_to "#", type: "button", onclick: "confirma_cobranca(#{boleto.id})", class: "btn btn-success btn-sm mb-5" do %>
  #         <i class="fa fa-check"></i> Cobrado?
  #       <% end %></td>        <%= link_to "#", type: "button", onclick: "confirma_cobranca(#{boleto.id})", class: "btn btn-success btn-sm mb-5" do %>
  #         <i class="fa fa-check"></i> Cobrado?
  #       <% end %></td>

  # Cria um link para editar
  # @param path [String] path de editar
  # @param options [String] path de editar
  def link_to_adicionar(path, options = {})
    link_to path,
            class: "btn btn-success  #{options[:class] || 'btn-xs'}",
            remote: options[:remote] || false,
            target: options[:target] || '_self',
            title: "Adicionar" do
      "#{ content_tag :span, class: 'fa fa-plus' do
        ''
      end} #{options[:label] if options.present? && options[:label]}".html_safe
    end
  end

  # Cria um link para editar
  # @param path [String] path de editar
  # @param options [String] path de editar
  def link_to_editar(path, options = {})
    link_to path,
            class: "btn btn-#{options[:btn_color] || 'warning'} #{options[:class] || 'btn-sm'}",
            title: "Editar",
            remote: options[:remote] || false,
            target: options[:target] || '_self',
            label: options[:label],
            style: "color: #FFF; " do
      "#{ content_tag :span, class: "fa fa-#{options[:fa_icon] || 'edit'}" do
        "#{options[:descricao] || ''}"
      end} #{options[:label] if options.present? && options[:label]}".html_safe
    end
  end

  # Cria um link para Excluir
  # @param path [String] path de excluir
  def link_to_excluir(path, options = {})
    link_to path,
            method: "delete",
            label: options[:label],
            class: "btn btn-danger #{options[:class] || 'btn-sm'}",
            remote: options[:remote] || false,
            title: options[:title] || "Excluir",
            data: { confirm: options[:confirm] || label_translate(:confirm_delete),
                    'confirm-button-text': "Confirmar",
                    'cancel-button-text': "Cancelar",
                    'confirm-button-color': "#66CD00",
                    'cancel-button-color': "#ff0000",
                    'sweet-alert-type': "warning",
            },
            style: "color: #FFF; float: none;" do
      "#{ content_tag :span, class: 'fa fa-trash' do
        ''
      end} #{options[:label] if options.present? && options[:label]}".html_safe
    end
  end

  # Cria um link para Cancelar
  # @param path [String] path de Cancelar
  def link_to_cancelar(path, options = {})
    link_to path,
            method: "delete",
            class: "btn btn-danger #{options[:class] || 'btn-xs'}",
            remote: options[:remote] || false,
            title: options[:title] || "Inativar",
            data: { confirm: options[:confirm] || label_translate(:confirm_cancel),
                    'confirm-button-text': "Confirmar",
                    'cancel-button-text': "Cancelar",
                    'confirm-button-color': "#66CD00",
                    'cancel-button-color': "#ff0000",
                    'sweet-alert-type': "warning",
            },
            style: "color: #FFF; float: none;" do
      "#{content_tag :span, class: "fa fa-ban" do
      end.html_safe}#{options[:btn_title]}".html_safe
    end
  end

  # Cria um link para um botão customizado
  # @param path [String] path da ação
  # @param path [String] path da ação
  def link_to_botao(path, title, icon, options = {})
    link_to path,
            class: "btn #{options[:class_button] || 'btn-info'}  #{options[:class] || 'btn-xs'}",
            title: title, remote: options[:remote] || false,
            method: options[:method] || :get,
            target: options[:target] || '_self',
            data: options[:confirm] ? { confirm: options[:confirm_text] || label_translate(:confirm_delete),
                                        'confirm-button-text': "Confirmar",
                                        'cancel-button-text': "Cancelar",
                                        'confirm-button-color': "#66CD00",
                                        'cancel-button-color': "#ff0000",
                                        'sweet-alert-type': "warning",
            } : nil,
            style: "color: #FFF; float: none;" do
      "#{ content_tag :span, class: "#{icon}" do
        "#{options[:descricao] || ''}"
      end} #{options[:label] if options.present? && options[:label]}".html_safe
    end
  end

  # Formata data
  def format_data(data)
    l(data, format: "%d/%m/%Y") if data.present?
  end

  # Retorna data formatada com hora
  def format_data_com_hora(data)
    l(data, format: "%d/%m/%Y %H:%M") if data.present?
  end

  # Retorna a classe bootstrap para estilizar mensagens de alerta
  def check_alert(name)
    case name
    when :notice
      'success'
    when :danger
      'danger'
    when :warning
      'warning'
    else
      'info'
    end
  end

  # Retorna o icone font-awesome para estilizar mensagens de alerta
  def check_alert_icon(name)
    case name
    when :danger
      'fa-exclamation-circle'
    when :warning
      'fa-question-circle'
    when :notice, :success
      'fa-check-circle'
    else
      'fa-info-circle'
    end
  end


  # Retorna os bytes da logo
  def logo_base_64
    'data:image/png;base64, ' + Base64.encode64(File.open("#{Rails.root}/app/assets/images/logo_orgao.png", 'rb').read).to_s
  end

  # Retorna os bytes da img
  def img_base_64(img)
    img = 'logo_orgao.png' unless File.exist?("#{Rails.root}/app/assets/images/#{img}")
    'data:image/png;base64, ' + Base64.encode64(File.open("#{Rails.root}/app/assets/images/#{img}", 'rb').read).to_s
  end

  # Retorna os bytes da img
  def img_base_64_path(path)
    Util.img_base_64_path path
  end

  #Enum no Select
  def options_from_enum_for_select(instance, enum)
    options_for_select(enum_collection(instance, enum), instance.send(enum))
  end

  def enum_collection(instance, enum)
    instance.class.send(enum.to_s.pluralize).keys.to_a.map { |key| [key.humanize, key] }.insert(-1, "Todos")
  end

  def formata_telefone(telefone)
    str_telefone = telefone.to_s
    if str_telefone.size == 11
      str_telefone.gsub(/(\d{2})(\d{5})(\d{4})/, '(\1)\2-\3')
    elsif str_telefone.size == 10
      str_telefone.gsub(/(\d{2})(\d{4})(\d{4})/, '(\1)\2-\3')
    elsif str_telefone.size == 9
      str_telefone.gsub(/(\d{5})(\d{4})/, '\1-\2')
    elsif str_telefone.size == 8
      str_telefone.gsub(/(\d{4})(\d{4})/, '\1-\2')
    else
      str_telefone
    end
  end

  def age(birthday)
    (Time.now.to_s(:number).to_i - birthday.to_time.to_s(:number).to_i) / 10e9.to_i
  end

end

# frozen_string_literal: true
# Classe base para os Finders
class BaseFinder
  attr_accessor :options

  # inicializa o objeto
  def initialize(options = {})
    @options = options
    @filtros = options[:filtros] if options[:filtros].present?
  end



  private



end

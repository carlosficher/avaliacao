# frozen_string_literal: true

class CreateCadastroBasicosTbpermissoes < ActiveRecord::Migration[5.2]
  def change
    unless table_exists? CadastroBasicos::Tbpermissao.table_name
      create_table CadastroBasicos::Tbpermissao.table_name do |t|
        t.string :classe
        t.string :acao
        t.string :nome
        t.integer :entidade
        t.integer :permissao_pai
        t.boolean :ativo
        t.string :chave
        t.timestamps
      end
    end
  end
end

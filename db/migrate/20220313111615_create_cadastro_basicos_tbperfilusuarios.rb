# frozen_string_literal: true

class CreateCadastroBasicosTbperfilusuarios < ActiveRecord::Migration[5.2]
  def change
    unless table_exists? CadastroBasicos::Tbperfilusuario.table_name
      create_table CadastroBasicos::Tbperfilusuario.table_name do |t|
        t.references :tbperfil, foreign_key: true
        t.references :admin, foreign_key: true
        t.integer :cdg_campeonato
        t.timestamps
      end
    end
  end
end

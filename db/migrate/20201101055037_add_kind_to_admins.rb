class AddKindToAdmins < ActiveRecord::Migration[5.2]
  def change
    add_column :admins, :kind, :integer
  end
end

# frozen_string_literal: true

class CreateCadastroBasicosMunicipes < ActiveRecord::Migration[5.2]
  def change
    unless table_exists? CadastroBasicos::Municipe.table_name
      create_table CadastroBasicos::Municipe.table_name do |t|
        t.string :nome_completo
        t.string :cpf
        t.integer :cns
        t.string :email
        t.date :dta_nascimento
        t.string :telefone
        t.string :foto
        t.boolean :status
        t.timestamps
      end
    end

  end


end

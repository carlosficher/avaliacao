# frozen_string_literal: true

class CreateCadastroBasicosTbperfilTbpermissoes < ActiveRecord::Migration[5.2]
  def change
    unless table_exists? CadastroBasicos::TbperfilTbpermissao.table_name
      create_table CadastroBasicos::TbperfilTbpermissao.table_name do |t|
        t.references :tbperfil, foreign_key: true
        t.references :tbpermissao, foreign_key: true

        t.timestamps
      end
    end
  end
end

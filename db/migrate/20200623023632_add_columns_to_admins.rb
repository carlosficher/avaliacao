class AddColumnsToAdmins < ActiveRecord::Migration[5.2]
  def change
    add_column :admins, :cpf, :string, limit: 20
    add_column :admins, :name, :string
    add_column :admins, :date_birth, :date
    add_column :admins, :date_register, :date
    add_column :admins, :active, :boolean
  end
end

# frozen_string_literal: true

class CreateCadastroBasicosTbperfis < ActiveRecord::Migration[5.2]
  def change
    unless table_exists? CadastroBasicos::Tbperfil.table_name
      create_table CadastroBasicos::Tbperfil.table_name do |t|
        t.string :nome
        t.boolean :ativo
        t.string :chave

        t.timestamps
      end
    end
  end
end

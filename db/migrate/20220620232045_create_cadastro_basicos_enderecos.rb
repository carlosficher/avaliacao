# frozen_string_literal: true

class CreateCadastroBasicosEnderecos < ActiveRecord::Migration[5.2]
  def change
    unless table_exists? CadastroBasicos::Endereco.table_name
      create_table CadastroBasicos::Endereco.table_name do |t|
        t.string :cep
        t.string :logradouro
        t.string :complemento
        t.string :bairro
        t.string :cidade
        t.string :uf
        t.string :cod_ibge
        t.references :municipes

        t.timestamps
      end
    end
  end
end

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_06_20_232045) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "admins", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "cpf", limit: 20
    t.string "name"
    t.date "date_birth"
    t.date "date_register"
    t.boolean "active"
    t.integer "kind"
    t.string "nmr_registro"
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
  end

  create_table "audits", force: :cascade do |t|
    t.integer "auditable_id"
    t.string "auditable_type"
    t.integer "associated_id"
    t.string "associated_type"
    t.integer "user_id"
    t.string "user_type"
    t.string "username"
    t.string "action"
    t.text "audited_changes"
    t.integer "version", default: 0
    t.string "comment"
    t.string "remote_address"
    t.string "request_uuid"
    t.datetime "created_at"
    t.index ["associated_type", "associated_id"], name: "associated_index"
    t.index ["auditable_type", "auditable_id", "version"], name: "auditable_index"
    t.index ["created_at"], name: "index_audits_on_created_at"
    t.index ["request_uuid"], name: "index_audits_on_request_uuid"
    t.index ["user_id", "user_type"], name: "user_index"
  end

  create_table "tbenderecos", force: :cascade do |t|
    t.string "cep"
    t.string "logradouro"
    t.string "complemento"
    t.string "bairro"
    t.string "cidade"
    t.string "uf"
    t.string "cod_ibge"
    t.bigint "municipes_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["municipes_id"], name: "index_tbenderecos_on_municipes_id"
  end

  create_table "tbmunicipes", force: :cascade do |t|
    t.string "nome_completo"
    t.string "cpf"
    t.integer "cns"
    t.string "email"
    t.date "dta_nascimento"
    t.string "telefone"
    t.string "foto"
    t.boolean "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tbperfil_tbpermissoes", force: :cascade do |t|
    t.bigint "tbperfil_id"
    t.bigint "tbpermissao_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tbperfil_id"], name: "index_tbperfil_tbpermissoes_on_tbperfil_id"
    t.index ["tbpermissao_id"], name: "index_tbperfil_tbpermissoes_on_tbpermissao_id"
  end

  create_table "tbperfilusuarios", force: :cascade do |t|
    t.bigint "tbperfil_id"
    t.bigint "admin_id"
    t.integer "cdg_campeonato"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["admin_id"], name: "index_tbperfilusuarios_on_admin_id"
    t.index ["tbperfil_id"], name: "index_tbperfilusuarios_on_tbperfil_id"
  end

  create_table "tbperfis", force: :cascade do |t|
    t.string "nome"
    t.boolean "ativo"
    t.string "chave"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tbpermissoes", force: :cascade do |t|
    t.string "classe"
    t.string "acao"
    t.string "nome"
    t.integer "entidade"
    t.integer "permissao_pai"
    t.boolean "ativo"
    t.string "chave"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "tbperfil_tbpermissoes", "tbperfis"
  add_foreign_key "tbperfil_tbpermissoes", "tbpermissoes"
  add_foreign_key "tbperfilusuarios", "admins"
  add_foreign_key "tbperfilusuarios", "tbperfis"
end

Rails.application.routes.draw do


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  concern :with_autocomplete do
    get "autocomplete", on: :collection
  end

  concern :with_update_table do
    get "update_table", on: :collection
  end

  namespace :cadastro_basicos do
    resources :enderecos, concerns: [:with_autocomplete, :with_update_table]
    resources :municipes, concerns: [:with_autocomplete, :with_update_table]
    resources :tbperfis, concerns: [:with_autocomplete, :with_update_table]
    resources :tbpermissoes, concerns: [:with_autocomplete, :with_update_table]

  end

  resources :audits, path: :auditoria, only: [:index]


  namespace :site do
    get 'home/index'
  end
  namespace :backoffice do
    get 'dashboard/index'
    get 'dashboard/inscricoes'

  end

  #para acessar apenas por backoffice
  get 'backoffice', to: 'backoffice/dashboard#index'

  resources :admin_usuarios, concerns: [:with_autocomplete] do
    get :inativar_admin, on: :member
  end

  # devise_for :admins
  devise_for :admins, controllers: {
    sessions: 'admins/sessions'
  }

  #root quando acessar sem especificar rota alguma
  root 'backoffice/dashboard#index'

end

module ActiveSupport
  class TimeWithZone
    def to_s_without_time_zone
      self.strftime('%d-%m-%Y %H:%M:%S')
    end

    def to_s_date
      self.strftime('%d/%m/%Y')
    end

    def to_s_br
      to_s_date
    end

    def to_s_br_time
      self.strftime('%d/%m/%Y %H:%M:%S')
    end

    def to_querry(sep = '-')
      if ActiveRecord::Base.connection.adapter_name == 'PostgreSQL'
        "to_date('#{self.to_s_without_time_zone}','dd#{sep}mm#{sep}yyyy hh24:mi:ss')"
      else
        "TRUNC(to_date('#{self.to_s_without_time_zone}','dd#{sep}mm#{sep}yyyy hh24:mi:ss'))"
      end
    end
  end

  def is_weekend?
    self.saturday? || self.sunday?
  end

  def beginning_of_month_util
    data = self.beginning_of_month
    if data.saturday?
      data = data + 2.day
    elsif data.sunday?
      data = data + 1.day
    end
    data
  end
end

class NilClass
  def to_date
    nil
  end

  def to_s_br
    nil
  end

  def to_s_br_time
    self.strftime('%d/%m/%Y  %H:%M:%S')
  end

  def is_weekend?
    nil
  end

  def beginning_of_month_util
    nil
  end
end

class DateTime
  def to_s_without_time_zone
    self.strftime('%d-%m-%Y %H:%M:%S')
  end

  def to_s_date
    self.strftime("%d/%m/%Y")
  end

  def to_s_br
    to_s_br_time
  end

  def to_querry(sep = '-')
    if ActiveRecord::Base.connection.adapter_name == 'PostgreSQL'
      "to_date('#{self.to_s_without_time_zone}','dd#{sep}mm#{sep}yyyy hh24:mi:ss')"
    else
      "TRUNC(to_date('#{self.to_s_without_time_zone}','dd#{sep}mm#{sep}yyyy hh24:mi:ss'))"
    end
  end

  def to_s_br_time
    self.strftime('%d/%m/%Y  %H:%M:%S')
  end

  def is_weekend?
    self.saturday? || self.sunday?
  end

  def beginning_of_month_util
    data = self.beginning_of_month
    if data.saturday?
      data = data + 2.day
    elsif data.sunday?
      data = data + 1.day
    end
    data
  end
end

class Time
  def to_s_without_time_zone
    self.strftime('%d-%m-%Y %H:%M:%S')
  end

  def to_s_br
    self.strftime('%d/%m/%Y  %H:%M:%S')
  end

  def to_s_br_only_time
    self.strftime('%H:%M')
  end

  def to_querry(sep = '-')
    if ActiveRecord::Base.connection.adapter_name == 'PostgreSQL'
      "to_date('#{self.to_s_without_time_zone}','dd#{sep}mm#{sep}yyyy hh24:mi:ss')"
    else
      "TRUNC(to_date('#{self.to_s_without_time_zone}','dd#{sep}mm#{sep}yyyy hh24:mi:ss'))"
    end
  end

  def r_date
    self.strftime("%d%m%Y")
  end

  def r_time
    self.strftime("%H%M%S")
  end

  def to_s_br_time
    self.strftime('%d/%m/%Y  %H:%M:%S')
  end

  def is_weekend?
    self.saturday? || self.sunday?
  end

  def beginning_of_month_util
    data = self.beginning_of_month
    if data.saturday?
      data = data + 2.day
    elsif data.sunday?
      data = data + 1.day
    end
    data
  end
end

class Date
  def self.valid_string?(str)
    Date.parse(str) rescue nil
  end

  def to_s_without_time_zone
    self.strftime('%d-%m-%Y')
  end

  def to_s_br
    self.strftime('%d/%m/%Y')
  end

  def to_querry(sep = '-')
    if ActiveRecord::Base.connection.adapter_name == 'PostgreSQL'
      "to_date('#{self.to_s_without_time_zone}','dd#{sep}mm#{sep}yyyy')"
    else
      "TRUNC(to_date('#{self.to_s_without_time_zone}','dd#{sep}mm#{sep}yyyy'))"
    end
  end

  def r_date
    self.strftime("%d%m%Y")
  end

  def months_between(date2)
    ((self.year * 12 + self.month) - (date2.year * 12 + date2.month)).abs
  end

  def to_s_br_time
    self.strftime('%d/%m/%Y  %H:%M:%S')
  end

  def is_weekend?
    self.saturday? || self.sunday?
  end

  def beginning_of_month_util
    data = self.beginning_of_month
    if data.saturday?
      data = data + 2.day
    elsif data.sunday?
      data = data + 1.day
    end
    data
  end
end

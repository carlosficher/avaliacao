# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')

Rails.application.config.assets.precompile += ["adminlte/jquery/jquery.min.js", "adminlte/bootstrap/js/bootstrap.bundle.min.js",
                                               "adminlte/js/adminlte.js", "adminlte/chart.js/Chart.min.js",
                                               "adminlte/js/pages/dashboard3.js",
                                               "adminlte/plugins/datatables/jquery.dataTables.min.js",
                                               "adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js",
                                               "adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js",
                                               "adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js",
                                               "adminlte/plugins/datatables-buttons/js/dataTables.buttons.min.js",
                                               "adminlte/plugins/datatables-buttons/js/buttons.bootstrap4.min.js",
                                               "adminlte/plugins/jszip/jszip.min.js",
                                               "adminlte/plugins/pdfmake/pdfmake.min.js",
                                               "adminlte/plugins/pdfmake/vfs_fonts.js",
                                               "adminlte/plugins/datatables-buttons/js/buttons.html5.min.js",
                                               "adminlte/plugins/datatables-buttons/js/buttons.print.min.js",
                                               "adminlte/plugins/datatables-buttons/js/buttons.colVis.min.js",
                                               "adminlte/plugins/jquery-mousewheel/jquery.mousewheel.js",
                                               "adminlte/plugins/raphael/raphael.min.js",
                                               "adminlte/plugins/jquery-mapael/jquery.mapael.min.js",
                                               "adminlte/plugins/jquery-mapael/maps/usa_states.min.js",
                                               "adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"
]

Rails.application.config.assets.precompile += %w( icon_tab.ico )
# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
# Rails.application.config.assets.precompile += %w( admin.js admin.css )
